import React from 'react';
import '../style/Card.css'

const Card = (props) => {
    return (
        <div className={`Card Card-rank-${props.rank} Card-${props.suit}`}>
            <span className="Card-rank">{props.rank}</span>
            <span className="Card-suit">{props.icon}</span>
        </div>
    );
};

export default Card;